## Rules

### We allow:

In short: Everything that's not outright bullying and/or illegal.

For example: Nobody will care if you use words like "nigger" or "faggot" here for the memes,
but using them for intentional bullying will be punished (see below).

This is not a safe-space for special snowflakes,
but it's not an unmoderated wasteland either.

Act like an adult and think before you type.

### We don't allow:

- **Any content that's illegal in United States**
- **Any content involving minors (human or not) in sexual poses/situations, revealing clothing, or naked (see below)**
- **Unflagged NSFW/NSFL Content**
- Gore and/or animal cruelty
- Targeted bullying/spamming
- Threatening or bullying someone for their opinion. (Just block/mute them.)
- Being a dick. (You know what that means.)
- Bot accounts, unless you have a written permission
- **Boosting stuff that's against our rules. (You'll be responsible for the boosted content.)**

### Content Involving Minors:

**"Henceforth and forevermore", posting (or boosting) any content involving minors (human or not) in sexual poses/situations,
revealing clothing, or naked, will be punished.
Posting or boosting said content will result in deletion and (depending on the severity) a permanent account ban.**

**Instances which frequently federate this kind of content to us will (depending on severity) have their media flagged or removed via MRF policies. 
In extreme cases, we will resort to full and permanent defederation.**

There is few room for discussion here.
I don't want to censor you, but I also don't want to go to jail for you.
Also, to be honest, I'd rather not pay the hosting bill for *that* kind of content, even if it was legal.
