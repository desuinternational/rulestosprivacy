# Terms Of Service

### 1. Terms

By accessing the website at https://desu.international, you are agreeing to be
bound by these terms of service, all applicable laws and regulations,
and agree that you are responsible for compliance with any applicable
local laws. If you do not agree with any of these terms, you are
prohibited from using or accessing this site. The materials contained in
this website are protected by applicable copyright and trademark law.

### 2. Disclaimer

The materials on desu.international's website are provided on an 'as is'
basis. desu.international makes no warranties, expressed or implied, and hereby
disclaims and negates all other warranties including, without
limitation, implied warranties or conditions of merchantability, fitness
for a particular purpose, or non-infringement of intellectual property
or other violation of rights. Further, desu.international does not warrant or
make any representations concerning the accuracy, likely results, or
reliability of the use of the materials on its website or otherwise
relating to such materials or on any sites linked to this site.

### 3. Limitations

In no event shall desu.international or its suppliers be liable for any damages
(including, without limitation, damages for loss of data or profit, or
due to business interruption) arising out of the use or inability to use
the materials on desu.international's website, even if desu.international or a
desu.international authorized representative has been notified orally or in
writing of the possibility of such damage. Because some jurisdictions do
not allow limitations on implied warranties, or limitations of liability
for consequential or incidental damages, these limitations may not apply
to you.

### 4. Accuracy of materials

The materials appearing on desu.international website could include technical,
typographical, or photographic errors. desu.international does not warrant that
any of the materials on its website are accurate, complete or current.
desu.international may make changes to the materials contained on its website at
any time without notice. However desu.international does not make any commitment
to update the materials.

### 5. Links

desu.international has not reviewed all of the sites linked to its website and
is not responsible for the contents of any such linked site. The
inclusion of any link does not imply endorsement by desu.international of the
site. Use of any such linked website is at the user's own risk.

### 6. Modifications

desu.international may revise these terms of service for its website at any
time, with notice via the "last modified" date on this page. By using
this website you are agreeing to be bound by the then current version of
these terms of service.

--

Last Modified: see git.
